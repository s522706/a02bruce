
QUnit.test("Here's a test that should always pass", function (assert) {
    assert.ok(1 <= "3", "1<3 - the first agrument is 'truthy', so we pass!");
});

QUnit.test('Testing ratio calculations.', function (assert){
  assert.equal( App.ratX(1920, 16 ,9), 1080, 'Tested X input with a standard ratio');
  assert.equal( App.ratY(1080, 16, 9), 1920, 'Tested Y input with a standard ratio');
  assert.equal( App.ratX(1920, 1920, 1080), 1080, 'Tested X input with a full resolution for ratio')
  assert.equal( App.ratY(1080, 1920, 1080), 1920, 'Tested X input with a full resolution for ratio')

});
