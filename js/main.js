
var App = {
  framerate: function(){
    var frForm = document.forms["frForm"];
    var dfr = frForm.elements["dfr"];
    var cfr = frForm.elements["cfr"];
    var frOutput;
  if(dfr.value!=""&&cfr.value!="")
   {
     if(!isNaN(parseFloat(dfr.value)))
     {
       if(!isNaN(parseFloat(cfr.value)))
       {
         frOutput = ((parseFloat(dfr.value)/parseFloat(cfr.value))*100 + "%");
         $("#frOut").html(frOutput);
         $("#frOut").css("color", "red");
         return(frOutput);
       }
       else
       {
         alert("Your current framerate is not a number!")
       }
     }
     else
     {
       alert("Your desired framerate is not a number!")
     }
   }
   else
   {
     alert("You have an empty field!")
     return("error");
   }

},
  ratio: function(){
    var ratForm = document.forms["ratForm"];
    var drx = ratForm.elements["drx"];
    var dry = ratForm.elements["dry"];
    var cwd = ratForm.elements["cwd"];
    var xOrY = ratForm.elements["selInput"];
    var selRadio;
    var ratOutput;

    for(var i=0; i<xOrY.length; i++)
    {
      if(xOrY[i].checked)
      {
        selRadio = xOrY[i].value;
        break;
      }
    }
    if(drx.value!=""&&dry.value!=""&&cwd.value!="")
     {
       if(!isNaN(parseFloat(drx.value)))
       {
         if(!isNaN(parseFloat(dry.value)))
         {
           if(!isNaN(parseFloat(cwd.value)))
           {
             if(selRadio == "0")
             {
               ratOutput = App.ratX(parseFloat(cwd.value),parseFloat(drx.value),parseFloat(dry.value));
               ratOutput = ratOutput.toFixed(0);
               $("#xDim").html(cwd.value);
               $("#yDim").html(ratOutput);
               $("#xDim").css("color", "red");
               $("#yDim").css("color", "red");
               return(ratOutput);
             }
             else if(selRadio == "1")
             {
               ratOutput = App.ratY(parseFloat(cwd.value),parseFloat(drx.value),parseFloat(dry.value));
               ratOutput = ratOutput.toFixed(0);
               $("#xDim").html(ratOutput);
               $("#yDim").html(cwd.value);
               return(ratOutput);
             }
           }
           else
           {
             alert("Your pixel count is not a number!");
           }
         }
         else
         {
           alert("Your Y ratio is not a number!");
         }
       }
       else
       {
         alert("Your X ratio is not a number!");
       }
     }
     else
     {
       alert("You have an empty field!");
     }
  },
  chooseWidth: function(){
    $("#WH").html("Width");
  },
  chooseHeight: function(){
    $("#WH").html("Height");
  },
  ratX: function(cwd,drx,dry)
  {
    return ((cwd*dry)/drx);
  },
  ratY: function(cwd,drx,dry)
  {
    return ((cwd*drx)/dry);
  }


}
